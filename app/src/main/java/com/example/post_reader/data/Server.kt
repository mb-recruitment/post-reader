package com.example.post_reader.data

import com.example.post_reader.data.response.PostsResponse
import io.reactivex.Single
import retrofit2.http.GET

interface Server {

    @GET("/posts")
    fun posts(): Single<List<PostsResponse>>
}