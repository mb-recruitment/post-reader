package com.example.post_reader.data.response

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class PostsResponse(
    val id: Int,
    val title: String,
    val body: String
)