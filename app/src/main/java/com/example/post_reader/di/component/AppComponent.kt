package com.example.post_reader.di.component

import android.app.Application
import com.example.post_reader.ReaderPostApplication
import com.example.post_reader.di.module.*
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidSupportInjectionModule::class,
        ActivityProviderModule::class,
        FragmentProviderModule::class,
        NetworkModule::class,
        AndroidModule::class,
        ViewModelModule::class
    ]
)
interface AppComponent : AndroidInjector<ReaderPostApplication> {

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }
}