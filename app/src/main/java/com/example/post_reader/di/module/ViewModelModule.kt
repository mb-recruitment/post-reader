package com.example.post_reader.di.module

import androidx.lifecycle.ViewModelProvider
import com.example.post_reader.di.InjectingViewModelFactory
import dagger.Binds
import dagger.Module

@Module
abstract class ViewModelModule {

    @Binds
    abstract fun bindViewModelFactory(factory: InjectingViewModelFactory): ViewModelProvider.Factory
}