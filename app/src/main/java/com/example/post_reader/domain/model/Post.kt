package com.example.post_reader.domain.model

data class Post(
    val id: Int,
    val title: String,
    val description: String
)