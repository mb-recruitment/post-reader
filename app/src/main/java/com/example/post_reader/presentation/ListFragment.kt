package com.example.post_reader.presentation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.post_reader.data.Server
import com.example.post_reader.databinding.FragmentListBinding
import com.example.post_reader.domain.model.Post
import dagger.android.support.DaggerFragment
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import javax.inject.Inject

class ListFragment : DaggerFragment() {

    @Inject
    lateinit var server: Server

    private lateinit var binding: FragmentListBinding
    private val disposables = CompositeDisposable()
    private val postsAdapter = PostsAdapter(emptyList())

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) =
        FragmentListBinding.inflate(inflater, container, false).apply {
            binding = this
        }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupPostsList()
        fetchData()
    }

    override fun onPause() {
        super.onPause()

        disposables.clear()
    }

    private fun fetchData() {
        server
            .posts()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(
                onSuccess = { postResponse ->
                    val posts = postResponse.asSequence()
                        .map { Post(id = it.id, title = it.title, description = it.body) }
                        .toList()

                    postsAdapter.newPosts(posts)
                },
                onError = { Timber.e(it) }
            )
            .addTo(disposables)
    }

    private fun setupPostsList() {
        binding.recyclerPosts.apply {
            layoutManager = LinearLayoutManager(requireContext())
            adapter = postsAdapter
        }
    }
}
