package com.example.post_reader.presentation

import android.os.Bundle
import androidx.fragment.app.FragmentActivity
import com.example.post_reader.R

class MainActivity : FragmentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)
    }
}