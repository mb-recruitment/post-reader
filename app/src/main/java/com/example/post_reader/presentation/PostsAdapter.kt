package com.example.post_reader.presentation

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.post_reader.R
import com.example.post_reader.domain.model.Post

class PostsAdapter(
    private var postsList: List<Post>
) : RecyclerView.Adapter<PostsViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        PostsViewHolder(
            LayoutInflater
                .from(parent.context)
                .inflate(R.layout.recycler_post_item, parent, false)
        )

    override fun getItemCount() = postsList.size

    override fun onBindViewHolder(holder: PostsViewHolder, position: Int) {
        holder.bind(postsList[position])
    }

    fun newPosts(newPosts: List<Post>) {
        postsList = newPosts
        notifyDataSetChanged()
    }
}